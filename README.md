# Simple Battery Welder Controller

This repository contains a reusable firmware for home-made battery spot welders
based around the ATiny25/45/85 microcontrollers.

Its role is to provide a simple user interface to adjust and read the welding
time as well as to execute the weld itself by sending a carefully timed signal
to the MOSFET drivers ( or whatever else the particular circuit uses ).

This firmware can easily be customized to suit pretty much any implementation.

## UI

UI consists of 2 digital inputs ( pedal+button ) and 2 digital outputs ( LEDs ).

Example implementation uses red and green LED.  

The RED LED indicates that the controller won't execute any welds if the pedal
is pressed, this can be due to the cooldown or the user interacting witht he UI.

The GREEN LED is used for the UI itself.

### Welding pulse time configuration & reading

When the BUTTON is pressed, the controller waits for it to be released.  
Pressing the button initiates IN_UI state which automatically disabled 
welding till the UI task is done.

If the BUTTON is released under a LONG_PRESS_TRESHOLD period, the welding pulse
time will be incremented and GREEN LED will blink once.  
If the welding time is over the upper limit, it will be reset to the lower limit
and GREEN LED will blink 3 times.

If the button is released after the LONG_PRESS_TRESHOLD period, the GREN LED
will blink out the welding time ( 1 blink = 1 ms ).

### Executing the welds

When the PEDAL is pressed and the welder is ready, the weld is executed.  
When the PEDAL is released, the cooldown period starts, during which the welder
won't execute any further welds.

If the pedal is accidentally pressed while the welder is not ready, the weld 
won't be executed, although the cooldown will be initiated again. This should
prevent accidental or unexpected welds.

## IO configuration

This is the default PIN assignment, feel free to change it in the code according
to your needs. While you are at it, you can as well change the welding pulse
lower and upper limits according to the power of your particular spot welder.
```
           _____
(RESET) --| |_| |-- VCC
 BUTTON --|     |-- MOSFET DRIVER GATE
  PEDAL --|     |-- GREEN LED+
    GND --|_____|-- RED LED+
```

## Example implementation

... TBA ...

## Compilation / flashing

Makefile is included and configured for the USBasp programmer and ATiny85.  
Feel free to change these according to your particular setup.

Packages needed to build & flash the firmware:
`build-essential` `gcc-avr` `binutils-avr` `avr-libc` `avrdude`