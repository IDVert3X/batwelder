/**
 * Copyright (c) 2018 Patrik Gajdoš
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 *
 *
 * The code should work on ATTiny 25/45/85 MCUs.s
 * Please adjust the Makefile for your chip and programmer combo accordingly.
 *
 *       IO configuration
 *            _____
 * (RESET) --| |_| |-- VCC
 *  BUTTON --|     |-- MOSFET DRIVERS
 *   PEDAL --|     |-- GREEN LED
 *     GND --|_____|-- RED LED
 *
 * If you use any other IO configuration, just edit the definitions below.
 *
 * TODO: use EEPROM to store weld_time
 */

#define F_CPU 1000000UL

#define PIN_RLED  (1 << PB0)
#define PIN_GLED  (1 << PB1)
#define PIN_FETS  (1 << PB2)
#define PIN_BTN   (1 << PB3)
#define PIN_PEDAL (1 << PB4)

#define BTN_COOLDOWN    200
#define PEDAL_COOLDOWN  500

#define LONG_PRESS_TRESHOLD 500

#define WELD_TIME_MIN 5
#define WELD_TIME_MAX 15

#define FLAG_IN_UI                  0x01
#define FLAG_BTN_RELEASE_PROCESSED  0x02
#define FLAG_PEDAL_PRESSED   0x04

#include <avr/io.h>
#include <avr/interrupt.h>

uint64_t last_pedal_release;
uint64_t last_button_press;
uint8_t flags;

uint8_t weld_time = WELD_TIME_MIN;

uint64_t _millis = 0;
uint16_t _1000us = 0;

// timer overflow occurs every 0.256 ms
ISR(TIM0_OVF_vect) {
        _1000us += 256;
        while (_1000us > 1000) {
            _millis++;
            _1000us -= 1000;
        }
}

// safe access to millis counter
uint64_t millis()
{
    uint64_t m;
    cli(); // disable interrupts while reading the value
    m = _millis;
    sei(); // enable interrupts
    return m;
}

void delay_ms(uint16_t ms)
{
    uint64_t start = millis();
    while (millis() < start + ms) {
    }
}

void blink_gled(uint8_t cnt, uint16_t delay)
{
    while (cnt-- > 0) {
        PORTB |= PIN_GLED;
        delay_ms(delay);
        PORTB &= ~PIN_GLED;
        delay_ms(delay);
    }
}

uint8_t is_welder_ready()
{
    return ((flags & FLAG_IN_UI) == 0) &&
           (flags & FLAG_PEDAL_PRESSED) == 0 &&
           (millis() > last_pedal_release + PEDAL_COOLDOWN);
}

void weld(uint16_t weld_time)
{
    PORTB |= PIN_FETS;
    delay_ms(weld_time);
    PORTB &= ~PIN_FETS;
}

void update_rled()
{
    if (is_welder_ready()) {
        PORTB &= ~PIN_RLED;
    } else {
        PORTB |= PIN_RLED;
    }
}

int main(void)
{
    // timer setup
    TCCR0B |= (1 << CS00); // use core clock ( 1:1 )
    TIMSK |= (1 << TOIE0); // enable timer overflow interrupt
    sei();                 // enable global interrupts

    // IO setup
    DDRB |= PIN_GLED | PIN_RLED | PIN_FETS; // outputs
    PORTB |= PIN_BTN | PIN_PEDAL; // pullups

    TCCR0A &= ~((1 << COM0A1) | (1 << COM0B1)); // disable PWM on pins PB0 and PB1

    flags |= FLAG_BTN_RELEASE_PROCESSED;

    for (;;) {
        if ((PINB & PIN_BTN) == 0) {
            if ((flags & FLAG_BTN_RELEASE_PROCESSED) && millis() > last_button_press + BTN_COOLDOWN) {
                last_button_press = millis();
                flags |= FLAG_IN_UI;
                flags &= ~FLAG_BTN_RELEASE_PROCESSED;
                update_rled();
            }
        } else if ((flags & FLAG_BTN_RELEASE_PROCESSED) == 0) {
            if (millis() > LONG_PRESS_TRESHOLD + last_button_press) {
                // long press
                blink_gled(weld_time, 250);
            } else {
                // short press
                if (weld_time < WELD_TIME_MAX) {
                    weld_time += 1;
                    blink_gled(1, 100);
                } else {
                    weld_time = WELD_TIME_MIN;
                    blink_gled(3, 100);
                }
            }
            flags |= FLAG_BTN_RELEASE_PROCESSED;
            flags &= ~FLAG_IN_UI;
            update_rled();
        }

        if ((PINB & PIN_PEDAL) == 0) {
            if (is_welder_ready()) {
                PORTB |= PIN_RLED;

                uint8_t pre_weld_pulse = weld_time / 8;
                if (pre_weld_pulse < 1) pre_weld_pulse = 1;

                weld(pre_weld_pulse);
                delay_ms(weld_time);
                weld(weld_time);
            }

            flags |= FLAG_PEDAL_PRESSED;
        } else if (flags & FLAG_PEDAL_PRESSED) {
            last_pedal_release = millis();
            flags &= ~FLAG_PEDAL_PRESSED;
        }

        update_rled();
    }
}
