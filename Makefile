PROGRAM     = batwelder
DEVICE      = attiny85
CLOCK       = 8000000
PROGRAMMER  = -c usbasp
OBJECTS     = $(PROGRAM).o
FUSES       = -U lfuse:w:0x62:m -U hfuse:w:0xdf:m -U efuse:w:0xff:m
AVRDUDE     = avrdude $(PROGRAMMER) -p $(DEVICE)
COMPILE     = avr-gcc -Wall -Os -mmcu=$(DEVICE)

all:	$(PROGRAM).hex

.c.o:
	$(COMPILE) -c $< -o $@

.S.o:
	$(COMPILE) -x assembler-with-cpp -c $< -o $@

.c.s:
	$(COMPILE) -S $< -o $@

flash:	all
	$(AVRDUDE) -U flash:w:$(PROGRAM).hex:i

fuse:
	$(AVRDUDE) $(FUSES)

make: flash fuse

load: all
	bootloadHID $(PROGRAM).hex

clean:
	rm -f $(PROGRAM).hex $(PROGRAM).elf $(OBJECTS)

$(PROGRAM).elf: $(OBJECTS)
	$(COMPILE) -o $(PROGRAM).elf $(OBJECTS)

$(PROGRAM).hex: $(PROGRAM).elf
	rm -f $(PROGRAM).hex
	avr-objcopy -j .text -j .data -O ihex $(PROGRAM).elf $(PROGRAM).hex
	avr-size --format=avr --mcu=$(DEVICE) $(PROGRAM).elf